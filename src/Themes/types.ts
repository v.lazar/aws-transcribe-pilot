export interface Theme {
    readonly name: string;
    readonly colors: {
        readonly base: {
            readonly primary: string;
            readonly secondary: string;
            readonly tertiary: string;
            readonly quaternary: string;
            readonly subtle: string;
        };
        readonly bg: {
            readonly primary: string;
            readonly secondary: string;
            readonly tertiary: string;
            readonly quaternary: string;
        };
        readonly semantic: {
            readonly light: string;
            readonly alarm: string;
            readonly cta: string;
            readonly ctaSecondary: string;
            readonly error: string;
            readonly hint: string;
            readonly success: string;
            readonly warning: string;
            readonly successBorder: string;
            readonly structuredText: string;
            readonly pocketParsedText: string;
        };
        readonly shadow: {
            readonly focus: string; // e.g. inputs, buttons, drawers and other focus states
            readonly shadow_01: string; // e.g. cards, fieldsets, carousel cards
            readonly shadow_02: string; // e.g. cards [hover], carousel cards [hover]
            readonly shadow_03: string; // modals
            readonly shadow_04: string; // e.g. dropdowns, tooltips, toast, drawers
        };
    };
    readonly sizes: {
        readonly gaps: {
            readonly base: string;
            readonly xxs: string;
            readonly xs: string;
            readonly s: string;
            readonly m: string;
            readonly l: string;
            readonly xl: string;
        };
    };
    readonly fonts: {
        readonly primary: {
            readonly family: string;
        };
        readonly secondary: {
            readonly family: string;
        };
        readonly tertiary: {
            readonly family: string;
        };
    };
}

export const enum ThemeName {
    Light = 'light',
    Dark = 'dark',
    Moon = 'moon',
}
