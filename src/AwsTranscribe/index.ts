// @ts-ignore
import mic from "microphone-stream";
import moo from "moo";
import crypto from "crypto"; // tot sign our pre-signed URL
import { Quill } from "quill";

import { EventStreamMarshaller } from "@aws-sdk/eventstream-marshaller"; // for converting binary event stream messages to and from JSON
import { toUtf8, fromUtf8 } from "@aws-sdk/util-utf8-node"; // utilities for encoding and decoding UTF8

import { downsampleBuffer, pcmEncode } from "./AudioUtils";
import { ChattyKathy } from "./ChattyKathy";
import { createPresignedURL } from "./AwsSignatureV4"; // to generate our pre-signed URL

const kathy = ChattyKathy();

// use transcribe medical or transcribe
const useMedical = true;

// our global variables for managing state
const accessKeyId = "AKIA6LUAKSJA6ULV3XHU";
const secretAccessKey = "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";
const sessionToken = "";

// our converter between binary event streams messages and JSON
const eventStreamMarshaller = new EventStreamMarshaller(toUtf8, fromUtf8);

const QUILL_EDITOR_ID = "sr-quill-editor";
// our global variables for managing state
const languageCode = "en-US";
const region = "eu-west-1";
const sampleRate = 44100;
let inputSampleRate = 16000;
// @ts-ignore
let transcription = "";
let socket: any;
let micStream: any;
let socketError = false;
let transcribeException = false;

interface VoiceIntegration {
  start: () => void;
  stop: () => void;
}

export class Transcribe implements VoiceIntegration {
  public async start() {
    return window.navigator.mediaDevices
      .getUserMedia({
        video: false,
        audio: true,
      })
      .then((arg) => streamAudioToWebSocket(arg));
  }

  public stop() {
    closeSocket();
  }
}

const streamAudioToWebSocket = (userMediaStream: any) => {
  //let's get the mic input from the browser, via the microphone-stream module
  micStream = new mic();

  micStream.on("format", (data: any) => {
    inputSampleRate = data.sampleRate;
  });

  micStream.setStream(userMediaStream);

  // Pre-signed URLs are a way to authenticate a request (or WebSocket connection, in this case)
  // via Query Parameters. Learn more: https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-query-string-auth.html
  const url = createPresignedUrl();

  //open up our WebSocket connection
  socket = new WebSocket(url);
  socket.binaryType = "arraybuffer";

  // when we get audio data from the mic, send it to the WebSocket if possible
  socket.onopen = () => {
    micStream.on("data", (rawAudioChunk: any) => {
      // the audio stream is raw audio bytes. Transcribe expects PCM with additional metadata, encoded as binary
      const binary = convertAudioToBinaryMessage(rawAudioChunk);

      if (socket.readyState === socket.OPEN) socket.send(binary);
    });
  };

  // handle messages, errors, and close events
  wireSocketEvents();
};

function wireSocketEvents() {
  // handle inbound messages from Amazon Transcribe
  socket.onmessage = ({ data }: any) => {
    //convert the binary event stream message to JSON
    const messageWrapper = eventStreamMarshaller.unmarshall(Buffer.from(data));
    const messageBody = JSON.parse(
      // @ts-ignore
      String.fromCharCode.apply(String, messageWrapper.body)
    );
    if (messageWrapper.headers[":message-type"].value === "event") {
      handleEventStreamMessage(messageBody);
    } else {
      transcribeException = true;
      console.error(messageBody.Message);
      showError(messageBody.Message);
    }
  };

  socket.onerror = () => {
    socketError = true;
    showError("WebSocket connection error. Try again.");
    // toggleStartStop();
  };

  socket.onclose = ({ code, reason }: any) => {
    micStream.stop();

    // the close event immediately follows the error event; only handle one.
    if (!socketError && !transcribeException) {
      if (code != 1000) {
        showError(`Streaming Exception: ${reason}`);
      }
      // toggleStartStop();
    }
  };
}

function handleEventStreamMessage(messageBody: any) {
  const results = messageBody.Transcript.Results;

  if (results.length > 0) {
    if (results[0].Alternatives.length > 0) {
      let transcript = results[0].Alternatives[0].Transcript;

      // fix encoding for accented characters
      transcript = decodeURIComponent(escape(transcript));

      // update the textarea with the latest result
      // $('#transcript').val(`${transcription + transcript}\n`);

      // if this transcript segment is final, add it to the overall transcription
      if (!results[0].IsPartial) {
        //scroll the textarea down
        // $('#transcript').scrollTop($('#transcript')[0].scrollHeight);

        transcription += transcript;

        const parsedText = lexAndParse(getQuill(), transcript);
        const quill = getQuill();

        if (quill) {
          const pos = quill.getSelection(true);
          if (pos) quill.insertText(pos.index, parsedText, "user");
        }
      }
    }
  }
}

const closeSocket = () => {
  if (socket != null && socket.readyState === socket.OPEN) {
    micStream.stop();

    // Send an empty frame so that Transcribe initiates a closure of the WebSocket after submitting all transcripts
    const emptyMessage = getAudioEventMessage(Buffer.from(new Buffer([])));
    // @ts-ignore
    const emptyBuffer = eventStreamMarshaller.marshall(emptyMessage);
    socket.send(emptyBuffer);
  }
};

function convertAudioToBinaryMessage(audioChunk: any) {
  const raw = mic.toRaw(audioChunk);

  if (raw == null) return;

  // downsample and convert the raw audio bytes to PCM
  const downsampledBuffer = downsampleBuffer(raw, inputSampleRate, sampleRate);
  const pcmEncodedBuffer = pcmEncode(downsampledBuffer);

  // add the right JSON headers and structure to the message
  const audioEventMessage = getAudioEventMessage(Buffer.from(pcmEncodedBuffer));

  //convert the JSON object + headers into a binary event stream message
  // @ts-ignore
  const binary = eventStreamMarshaller.marshall(audioEventMessage);

  return binary;
}

function getAudioEventMessage(buffer: any) {
  // wrap the audio data in a JSON envelope
  return {
    headers: {
      ":message-type": {
        type: "string",
        value: "event",
      },
      ":event-type": {
        type: "string",
        value: "AudioEvent",
      },
    },
    body: buffer,
  };
}

function createPresignedUrl() {
  const endpoint = `transcribestreaming.${region}.amazonaws.com:8443`;

  if (useMedical) {
    return createPresignedURL(
      "GET",
      endpoint,
      "/medical-stream-transcription-websocket",
      "transcribe",
      crypto.createHash("sha256").update("", "utf8").digest("hex"),
      {
        key: accessKeyId,
        secret: secretAccessKey,
        sessionToken: sessionToken,
        protocol: "wss",
        expires: 15,
        region,
        query: `language-code=${languageCode}&media-encoding=pcm&sample-rate=${sampleRate}&specialty=PRIMARYCARE&type=DICTATION`,
      }
    );
  } else {
    // get a preauthenticated URL that we can use to establish our WebSocket
    return createPresignedURL(
      "GET",
      endpoint,
      "/stream-transcription-websocket",
      "transcribe",
      crypto.createHash("sha256").update("", "utf8").digest("hex"),
      {
        key: accessKeyId,
        secret: secretAccessKey,
        sessionToken,
        protocol: "wss",
        expires: 15,
        region,
        query: `language-code=${languageCode}&media-encoding=pcm&sample-rate=${sampleRate}&specialty=PRIMARYCARE&type=DICTATION`,
      }
    );
  }
}

function showError(msg: string) {
  window.alert(msg);
}

const lexer = moo.compile({
  openReport: /[o|O]pen [R|r]eport?./,
  closeReport: /[c|C]lose [R|r]eport?./,
  resetReport: /[r|R]eset?. [R|r]eport?./,

  goToTop: /[g|G]o to(?: the)? [t|T]op?./,
  goToBottom: /[g|G]o to(?: the)? [b|B]ottom?./,

  newLine: /[n|N]ew [L|l]ine?.(?:\s)?/,
  newParagraph: /[n|N]ew [P|p]aragraph?.(?:\s)?/,

  fullStop: /[f|F]ull [S|s]top?./,
  period: /[p|P]eriod?./,
  comma: /[c|C]omma?./,
  colon: /[c|C]olon?./,

  openBracket: /[o|O]pen [b|B]racket?./,
  closeBracket: /[c|C]lose [b|B]racket?./,

  undo: /[U|u]nd(?:o|ue)\.?/,
  redo: /[R|r]edo?./,

  text: moo.fallback,
});

function lexText(lexer: moo.Lexer, str: string) {
  const tokens: Array<moo.Token> = Array.from(lexer.reset(str));

  return tokens;
}

export function lexAndParse(quill: QuillWithHistory | null, str: string) {
  const tokens = lexText(lexer, str);

  const text = parseTokens(tokens, quill);
  console.log("parsed text");
  console.log(text);
  return text;
}

function closeReport() {
  kathy
    .SpeakWithPromise("Okay Michael, I am gonna close the report")
    .catch(console.log);
}

function openReport() {
  kathy
    .SpeakWithPromise("Okay Michael, I am gonna open a new report")
    .catch(console.log);
}

function resetReport(quill: Quill) {
  const transLenght = quill.getText().length;

  quill.deleteText(0, transLenght);
}

function goToTop(quill: Quill) {
  const range = { index: 0, length: 0 };
  quill.setSelection(range);
}
function goToBottom(quill: Quill) {
  const transLenght = quill.getText().length;

  const range = { index: 0, length: transLenght };
  quill.setSelection(range);
}

interface QuillWithHistory extends Quill {
  history: {
    undo: () => void;
    redo: () => void;
  };
}

function undo(quill: QuillWithHistory) {
  quill.history.undo();
}
function redo(quill: QuillWithHistory) {
  quill.history.redo();
}

export function getQuill(): QuillWithHistory | null {
  const transcriptQuill = document.getElementById(QUILL_EDITOR_ID) as any;
  const quill = transcriptQuill?.__quill as QuillWithHistory;

  return quill;
}

function parseTokens(
  tokens: moo.Token[],
  quill: QuillWithHistory | null
): string {
  let text = "";
  for (const tok of tokens) {
    console.log(tok);
    switch (tok.type) {
      case "openReport": {
        openReport();
        console.log("recognized command open report");
        break;
      }
      case "closeReport": {
        closeReport();
        console.log("recognized command close report");
        break;
      }

      case "resetReport":
        if (quill) resetReport(quill);
        break;
      case "goToTop":
        if (quill) goToTop(quill);
        break;
      case "goToBottom":
        if (quill) goToBottom(quill);
        break;
      case "newLine":
      case "newParagraph":
        text = text.trimRight() + "\n";
        break;
      case "fullStop":
      case "period":
        text += ".";
        break;
      case "comma":
        text += ",";
        break;
      case "colon":
        text += ":";
        break;
      case "openBracket":
        text += "(";
        break;
      case "closeBracket":
        text += ")";
        break;
      case "undo":
        if (quill) undo(quill);
        break;
      case "redo":
        if (quill) redo(quill);
        break;
      default:
        if (
          tok.text.trim() !==
          "Okay, Michael, I am going to open the new report."
        )
          text += tok.text + " ";
    }
  }
  return text;
}
