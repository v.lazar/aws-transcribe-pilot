import { Theme, ThemeName } from './types';

export const lightTheme: Theme = {
    name: ThemeName.Light,
    colors: {
        base: {
            primary: '#171E26',
            secondary: '#424B57',
            tertiary: '#718094',
            quaternary: '#A9B8CB',
            subtle: '#D5DDE7',
        },
        bg: {
            primary: '#F2F5F8',
            secondary: '#FFFFFF',
            tertiary: '#E8EDF2',
            quaternary: '#D5DEE7',
        },
        semantic: {
            light: '#FFFFFF',
            cta: '#125DDE',
            ctaSecondary: '#2697ED',
            hint: '#9C9011',
            success: '#2AA73F',
            successBorder: '#2AA73F',
            warning: '#D08011',
            alarm: '#F25A66',
            error: '#D9121C',
            structuredText: '#F2F5F8',
            pocketParsedText: 'rgba(250,250,210,1)',
        },
        shadow: {
            focus: '0 0 0 2px rgba(38,151,237,0.25), 0 0 4px 0 rgba(38,151,237,0.50)', // light blue
            shadow_01: '0 1px 2px 0 rgba(66,75,87,0.03)', // fg-secondary
            shadow_02: '0 4px 6px 0 rgba(66,75,87,0.05)', // fg-secondary
            shadow_03: '0 10px 20px 0 rgba(0,0,0,0.10)',
            shadow_04: '0 4px 8px 0 rgba(23,30,38,0.04), 0 12px 20px 0 rgba(23,30,38,0.04)', // fg-primary
        },
    },
    sizes: {
        gaps: {
            base: '4px',
            xxs: '8px',
            xs: '16px',
            s: '24px',
            m: '32px',
            l: '40px',
            xl: '48px',
        },
    },
    fonts: {
        primary: { family: 'IBM Plex Sans' },
        secondary: { family: 'iA Writer Duospace' },
        tertiary: { family: 'IBM Plex Serif' },
    },
};
