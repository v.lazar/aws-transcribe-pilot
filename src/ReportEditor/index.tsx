import React from "react";
import { ThemeProvider } from "styled-components";

import { RichText } from "@smartreporting/rich-text";
import { TemplateInterface } from "@smartreporting/template";
import { ReportInterface } from "@smartreporting/report";
import {
  EditorWrapper,
  QuillEditor,
  QuillProxyInterface,
  QuillRange,
  QuillToolbar,
  QuillWrapper,
  Sidebar,
  StructureEditingActions,
  useEngine,
  useNuanceSpeechRecognition,
  useStructureEditingReducer,
} from "@smartreporting/report-editor";

import { lightTheme } from "../Themes/light";
//import { Transcribe } from "../AwsTranscribe";
import { NuanceDevLicense } from "../Nuance";

export function ReportEditor(props: {
  readonly report: ReportInterface;
  readonly template: TemplateInterface;
}) {
  const [state, dispatch] = useStructureEditingReducer(
    props.report,
    props.template
  );
  const quillProxyRef = React.useRef<QuillProxyInterface>();
  const toolbarRef = React.createRef<HTMLDivElement>();
  const storeQuillState = (document: RichText, range?: QuillRange) =>
    dispatch(StructureEditingActions.updateQuillState(document, range));

  useEngine(state, dispatch, ENGINE_DEBOUNCE_DELAY_IN_MILLISECONDS);

  useNuanceSpeechRecognition(
    quillProxyRef.current?.getEditorDomNode(),
    NuanceDevLicense,
    alert
  );

  // React.useEffect(() => {
  //   new Transcribe().start();
  // }, []);

  return (
    <ThemeProvider theme={lightTheme}>
      <EditorWrapper>
        <Sidebar
          report={state.report}
          template={state.template}
          dispatch={dispatch}
          focusedPath={state.sidebarNavigation.focusedPath}
          lowestExpandedPath={state.sidebarNavigation.lowestExpandedPath}
        />
        <QuillWrapper>
          <QuillToolbar toolbarRef={toolbarRef}></QuillToolbar>
          <QuillEditor
            toolbarRef={toolbarRef}
            document={state.quillDocument}
            range={state.quillRange}
            onDocumentChange={storeQuillState}
            onRangeChange={storeQuillState}
            quillRef={quillProxyRef}
            focusedPath={state.sidebarNavigation.focusedPath}
          />
        </QuillWrapper>
      </EditorWrapper>
    </ThemeProvider>
  );
}

const ENGINE_DEBOUNCE_DELAY_IN_MILLISECONDS = 1000;
