import { Theme, ThemeName } from './types';

export const darkTheme: Theme = {
    name: ThemeName.Dark,
    colors: {
        base: {
            primary: '#DBE1E6',
            secondary: '#A5B3C0',
            tertiary: '#718294',
            quaternary: '#384551',
            subtle: '#262E36',
        },
        bg: {
            primary: '#0D0F12',
            secondary: '#13171B',
            tertiary: '#1B2227',
            quaternary: '#283139',
        },
        semantic: {
            light: '#EEF1F5',
            cta: '#2D69D2',
            ctaSecondary: '#68B4EE',
            hint: '#E4D63F',
            success: '#4DCB62',
            successBorder: '#2AA73F',
            warning: '#E9A03A',
            alarm: '#EF717B',
            error: '#CC333B',
            structuredText: '#0D0F12',
            pocketParsedText: 'rgba(250,250,210,1)',
        },
        shadow: {
            focus: '0 0 4px 0 rgba(104,180,238,0.50), 0 0 0 2px rgba(104,180,238,0.25)',
            shadow_01: '0 1px 2px 0 rgba(0,0,0,1)',
            shadow_02: '0 1px 3px 0 rgba(0,0,0,1)',
            shadow_03: '0 10px 20px 0 rgba(0,0,0,0.33)',
            shadow_04: '0 4px 8px 0 rgba(0,0,0,0.50), 0 12px 20px 0 rgba(0,0,0,0.50)',
        },
    },
    sizes: {
        gaps: {
            base: '4px',
            xxs: '8px',
            xs: '16px',
            s: '24px',
            m: '32px',
            l: '40px',
            xl: '48px',
        },
    },
    fonts: {
        primary: { family: 'IBM Plex Sans' },
        secondary: { family: 'iA Writer Duospace' },
        tertiary: { family: 'IBM Plex Serif' },
    },
};
