import "../static/css/fonts.css";
import "../static/css/normalize.css";

import { Report } from "@smartreporting/report";
import { Locale } from "@smartreporting/report-editor";
import { QuillDeltaRichText } from "@smartreporting/rich-text";
import { TemplateDescription } from "@smartreporting/template";

import React from "react";
import ReactDOM from "react-dom";

import { ReportEditor } from "./ReportEditor";

const report = new Report("", Locale.AmericanEnglish, { id: -1, version: -1 });
const template = TemplateDescription.begin(QuillDeltaRichText.empty)
  .tree((root) =>
    root.child((topLevel) =>
      topLevel
        .nameAndOutput("Findings", { bold: true })
        .followedByColon()
        .child((nav) =>
          nav
            .nameAndOutput("Cranial area", { bold: true })
            .onNewline()
            .followedByColon()
            .child((q) =>
              q
                .name("Internal bleeding")
                .onlyOneActiveChildAllowed()
                .child((ans) =>
                  ans.nameAndOutput("No evidence of cranial bleeding")
                )
                .child((ans) =>
                  ans.nameAndOutput("Evidence of cranial bleeding")
                )
            )
        )
    )
  )
  .finalize();

function App() {
  return <ReportEditor report={report} template={template} />;
}

ReactDOM.render(<App />, document.getElementById("report-editor"));
