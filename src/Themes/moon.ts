import { Theme, ThemeName } from './types';

export const moonTheme: Theme = {
    name: ThemeName.Moon,
    colors: {
        base: {
            primary: '#FFFFFF',
            secondary: '#E8E8E8',
            tertiary: '#A5A5A5',
            quaternary: '#787878',
            subtle: '#4B4B4B',
        },
        bg: {
            primary: '#1E1E1E',
            secondary: '#282828',
            tertiary: '#323232',
            quaternary: '#3C3C3C',
        },
        semantic: {
            light: '#FFFFFF',
            cta: '#CF4B00',
            ctaSecondary: '#FFFFFF',
            hint: '#FFD200',
            success: '#009A38',
            successBorder: '#009A38',
            warning: '#FFD200',
            alarm: '#E7001D',
            error: '#E7001D',
            structuredText: 'rgba(255,255,255,0.1)',
            pocketParsedText: 'rgba(255,210,0,0.2)',
        },
        shadow: {
            focus: '0 0 4px 0 rgba(104,180,238,0.50), 0 0 0 2px rgba(104,180,238,0.25)',
            shadow_01: '0 1px 2px 0 rgba(0,0,0,1)',
            shadow_02: '0 1px 3px 0 rgba(0,0,0,1)',
            shadow_03: '0 10px 20px 0 rgba(0,0,0,0.33)',
            shadow_04: '0 4px 8px 0 rgba(0,0,0,0.50), 0 12px 20px 0 rgba(0,0,0,0.50)',
        },
    },
    sizes: {
        gaps: {
            base: '4px',
            xxs: '8px',
            xs: '16px',
            s: '24px',
            m: '32px',
            l: '40px',
            xl: '48px',
        },
    },
    fonts: {
        primary: { family: 'Siemens Sans' },
        secondary: { family: 'Siemens Sans' },
        tertiary: { family: 'Siemens Sans' },
    },
};
